Standard Project Repository
========================================================

A standardised project repository layout with documentation, Gitlab CI/CD
and code testing.


.. toctree::
   :maxdepth: 2

   standard_project
   python_docs
