.. _python:

Autogen Python Docs
=====================

   This code is written in *Python 3.8*.

   It uses the docstring comments within the ``standard_project.py`` file to
   automatically document the Python code.

``standard_project.py``
--------------------------

.. automodule:: src.standard_project.standard_project
   :members:
