.. _std_proj:

Standard Project
------------------------------

This project is intended to be used as a living template to facilitate arranging new projects.
It is intended to include basic structure required to:

* generate documentation from restructuredText
* auto generating docs from source code using doxygen
* making use of Gitlab Pages to present docs once built
* automated testing of code
* make use of Gitlab CI/CD pipelines for building docs and testing
* enforces linting best practices (mypy/pylint)
* uses Poetry to manage Python dependencies


Getting started
~~~~~~~~~~~~~~~~~

Follow these simple instructions for new projects.
You can either clone this repository for yourself or use it as a base for your new repo.

Clone GIT repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Clone this repository onto the machine machine using the git command line.

 .. code-block:: bash

   git clone git@gitlab.com:piatra-automation/standard_project.git

Install Poetry dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Then using Makefile to install the poetry dependencies.
This will install the required linters, pytest, mypy and other tooling that will be used by the project.

.. code-block:: bash

  cd /standard_project
  sudo make install-dev

Activate virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Finally, you can activate the virtual environment managed by Poetry to ensure that tools are available from the VS Code IDE.

.. code-block:: bash

  poetry shell
  code .

Gitlab CI/CD
~~~~~~~~~~~~~~~~~

This project will use the Gitlab CI/CD runners to build upon a commit.
The rules for this CI/CD setup is located in ``.gitlab-ci.yml``.

It is configured to:

* analyse the code for linting and stylistic conventions using mypy and pylint
* run automated testing steps conatined in the ``tests`` folder
* build the Sphinx documentation from source in the ``docs`` folder
* move the generated docs to the projects Gitlab Pages folder


License
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is all released under the MIT open source license
